// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

// https://dhomes@bitbucket.org/dhomes/spm_cpcloudkit.git
import PackageDescription

let version = "1.0.6"

let package = Package(
    name: "CPCloudKit",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "CPCloudKit",
            targets: ["CPCloudKit"]),
    ],
    dependencies: [
        .package(url: "https://bitbucket.org/dhomes/spm_cpcorekit.git", from: "1.0.0")
    ],
    targets: [
        .binaryTarget(
            name: "CPCloudKit",
            url: "https://bitbucket.org/dhomes/spm_cpcloudkit/downloads/CPCloudKit.xcframework.zip",
            checksum: "5c4550cb908fcc042568c5fe1db259b659f138ea62d6c43ae66bf75dbf880135"	
        ),
		.target(
		name: "CloudKitWrapper",
		dependencies: [
			  "spm_cpcorekit"
			],
        linkerSettings: [
          	.linkedFramework("spm_cpcorekit")
          ]
		)
    ]
)
